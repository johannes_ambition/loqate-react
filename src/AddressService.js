const findEndpoint = "https://api.addressy.com/Capture/Interactive/Find/v1.10/json3.ws";
const retriveEndpoint = "https://api.addressy.com/Capture/Interactive/Retrieve/v1/json3.ws";
const batchCleanserEndpoint = "https://api.addressy.com/Cleansing/International/Batch/v1.00/json4.ws";

let apiKey = process.env.REACT_APP_LOQATE_API_KEY;

const find = (textValue, country, container = "", limit = 200) => {

	//console.log (" AddressService > country = " , country);

	let params = '';
	params += "&Key=" + encodeURIComponent(apiKey);
	params += "&Text=" + encodeURIComponent(textValue);
	//params += "&IsMiddleware=" + encodeURIComponent(IsMiddleware);
	params += "&Container=" + encodeURIComponent(container);
	params += "&Origin=" + encodeURIComponent(country);
	params += "&Countries=" + encodeURIComponent(country);
	params += "&Limit=" + encodeURIComponent(limit);
	//params += "&Language=" + encodeURIComponent(country.toLowerCase());
	params += "&Language=" + encodeURIComponent("en");

	const credentials = {
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: params
	};

	return fetch(findEndpoint + "?" + credentials.body)
		.then((res) => {
			return res.json()
		}).then((res) => {
			return res
		})

};

const retrive = (id) => {

	let params = '';
	params += "&Key=" + encodeURIComponent(apiKey);
	params += "&Id=" + encodeURIComponent(id);

	const credentials = {
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: params
	}

	return fetch(retriveEndpoint + "?" + credentials.body)
		.then((res) => {
			return res.json()
		}).then((res) => {
			return res
		})
		.catch((err) => {
			//console.log(" AddressLookUp > err = ", err);
		})

};


const batchCleanser = ({ Locality = "", PostalCode = "", Street = "", Country = "", Premise = "" }) => {
	let payload = {
		"Key": apiKey,
		"Options": {
			"Process": "Search",
			"ServerOptions": { "MaxResults": 200, "OutputScript": "Native", "OutputAddressFormat": "true", "MinimumGeoAccuracyLevel": "5" }
		},
		"Addresses": [
			{
				Country,
				"SuperAdministrativeArea": "",
				"AdministrativeArea": "",
				"SubAdministrativeArea": "",
				Locality,
				"DependentLocality": "",
				"DoubleDependentLocality": "",
				"Thoroughfare": Street,
				"DependentThoroughfare": "",
				"Building": "",
				Premise,
				"SubBuilding": "",
				PostalCode,
				"Organization": "",
				"PostBox": ""
			}
		]
	}

	const init = {
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify(payload)
	};

	return fetch(batchCleanserEndpoint, init)
		.then((res) => {
			return res.json()
		}).then((res) => {
			return res[0]
		})
}



export { find, retrive, batchCleanser }
