import React, { useState } from 'react'
import Autocomplete from './Autocomplete'
import { find, retrive, batchCleanser } from './AddressService'
import {
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@material-ui/core'


const App = ({ text, fieldValues }) => {

  const [countryCode, setCountryCode] = useState('BE')

  const autoCompleteMenuStyles = {
    border: "1px solid #e8e8e8",
    borderRadius: '5px',
    boxShadow: '4px 3px 8px 0 rgba(0, 0, 0, 0.1)',
    background: 'rgba(255, 255, 255, 0.9)',
    maxHeight: 200,
    overflowY: "auto",
    position: 'absolute',
    left: '0',
    zIndex: '5',
    padding: '0px',
    top: '50px',
  }

  const [PostalCode, setPostalCode] = useState('')
  const [StreetAddress, setStreetAddress] = useState('')
  const [City, setCity] = useState('')
  const [HouseNumber, setHouseNumber] = useState('')

  const [streetQuery, setStreetQuery] = useState('')
  const [suggestedStreets, setSuggestedStreets] = useState([])
  const [selectedStreet, setSelectedStreet] = useState('')

  const [houseNumberQuery, setHouseNumberQuery] = useState('')
  const [suggestedHouseNumbers, setSuggestedHouseNumbers] = useState([])

  const compoundQuery = [PostalCode, City].join(' ').trim();
  const displayedCompoundQuery = [PostalCode, City, streetQuery, houseNumberQuery].join(' ').trim();

  const clearActiveQueries = (until) => {
    switch (until) {
      case "zip":
        setSelectedStreet('')
        setHouseNumberQuery('')
        setStreetQuery('')

        setStreetAddress('')
        setStreetAddress('')
        setHouseNumber('')
        break;
      case "street":
        setSelectedStreet('')
        setHouseNumberQuery('')
        setHouseNumber('')
        break;
      case "houseNumber":
        setHouseNumberQuery('')
        break;

      default:
        break;
    }
  }

  const postcodeChangeHandler = e => {
    const query = e.target.value
    setPostalCode(query)

    clearActiveQueries('zip')
    find(query, countryCode).then(res => {
      const postalCodeSuggestions = res.Items.filter(item => item.Type === 'Postcode')

      if (postalCodeSuggestions.length > 0) {
        let mightBeACityName = postalCodeSuggestions[0].Description.split('-')[0].trim()
        let isLikelyCityName = mightBeACityName.indexOf('Addresses') === -1
        setCity(isLikelyCityName ? mightBeACityName : ' ')
      } else {
        setCity('')
      }
    })
  }

  const streetChangeHandler = e => {
    setStreetAddress(e.target.value)
    setStreetQuery(e.target.value)

    clearActiveQueries('street')
    find(compoundQuery + ' ' + e.target.value, countryCode, '', 15).then(res => {
      let itemsOfTypeStreet = res.Items.filter(item => item.Type === 'Street')

      // Prune duplicates
      let map = {}
      let suggestedStreetsWithoutDuplicates = []
      itemsOfTypeStreet.forEach(item => {
        if (!map[item.Text]) {
          // Check if better candidate
          map[item.Text] = item
        }
      })

      for (const suggestion in map) {
        suggestedStreetsWithoutDuplicates.push(map[suggestion])
      }

      setSuggestedStreets(suggestedStreetsWithoutDuplicates)
    })
  }

  const streetSelectHandler = (id, item) => {
    setStreetAddress(item.Text)
    setStreetQuery(item.Text)
    setSelectedStreet(item.Text)

     // If city name is not found by postal code. Typical for Austria and Belgium
     if (!City.trim()) {
      batchCleanser({ PostalCode, Street: item.Text, Country: countryCode }).then((res) => {
        const { Matches } = res
        const first = Matches.find(match => match.PostalCode && match.PostalCode === PostalCode)
        setCity('City', first.Locality || '')
      })
    }
  }

  const houseNumberChangeHandler = e => {
    setHouseNumberQuery(e.target.value)

    find([compoundQuery, StreetAddress, e.target.value].join(''), countryCode, '', 15).then(res => {
      // setSuggestedStreets(res.Items.filter(item => item.Type === 'Address' || item.Type === 'Street'))
      console.log('Housenumber response', res);
      let relevantResults = res.Items.filter(item => item.Type === "Address" && item.Description.indexOf(PostalCode) !== -1)
      relevantResults.forEach(result => result.Text = result.Text.replace(StreetAddress, ''))
      setSuggestedHouseNumbers(relevantResults)
    })

  }

  const houseNumberSelectHandler = (id, item) => {
    // The select address is now a full address we can retrieve and use to fill out the form
    retrive(id).then((res) => {
      const address = res.Items[0]
      setHouseNumberQuery(address.BuildingNumber)

      setStreetAddress(address.Street || '')
      setHouseNumber(address.BuildingNumber || '')
      setPostalCode(address.PostalCode || '')
      setCity(address.City || '')
    })

  }

  return (
    <>
      <h3>Suggested queries:</h3>
      <ul>
        <li>Ernst-August-Platz 5, 30159 Hannover, DE</li>
        <li>Mozartlaan 17, 4837 EH Breda, NL</li>
        <li>Rue Dr Colson 11, 1430 Rebecq, BE <span style={{
          fontSize: ".8rem",
          color: "grey"
        }}>(Rue du Docteur Colson)</span>

        </li>
      </ul>

      <h2>Form:</h2>
      <FormControl >
        <InputLabel >Country</InputLabel>
        <Select
          id="demo-simple-select"
          value={countryCode}
          onChange={e => setCountryCode(e.target.value)}
        >
          <MenuItem value={'NL'}>NL</MenuItem>
          <MenuItem value={'DE'}>DE</MenuItem>
          <MenuItem value={'AT'}>AT</MenuItem>
          <MenuItem value={'BE'}>BE</MenuItem>
        </Select>
      </FormControl>

      <div className="sof-field-row">


        <div className="sof-field-container pr-1 w-50">

          <TextField name="PostalCode" id="PostalCode" type="text" onChange={postcodeChangeHandler} value={PostalCode} label={text.Zipcode} />
        </div>

        <div className="sof-field-container w-50">
          <TextField type="text" autoComplete="nope" pattern="[A-ZÆØÅÄÖÜÉa-zæøåäöüé ]+" name="City" id="City" value={City} required readOnly disabled label={text.City} />
        </div>
      </div>

      <div className="sof-field-row">

        <Autocomplete
          getItemValue={item => item.Id}
          value={streetQuery}
          onChange={streetChangeHandler}
          onSelect={streetSelectHandler}
          wrapperProps={{ className: "sof-field-container col-md-12" }}
          menuStyle={{ ...autoCompleteMenuStyles, zIndex: 15 }}
          items={suggestedStreets}
          renderInput={props => <TextField label={text.Street} type="text" autoComplete="nope" name="street" {...props} disabled={!City} />}
          renderItem={(item, isHighlighted) => (
            <div key={item.Id} style={{ background: isHighlighted ? 'lightgray' : 'white', padding: "10px 15px", borderBottom: "1px solid #e8e8e8" }}>
              {item.Text}
            </div>
          )}
        />

      </div>

      <div className="sof-field-row">

        <Autocomplete
          getItemValue={item => item.Id}
          value={houseNumberQuery}
          onChange={houseNumberChangeHandler}
          onSelect={houseNumberSelectHandler}
          wrapperProps={{ className: "sof-field-container col-md-6 pr-1" }}
          menuStyle={{ ...autoCompleteMenuStyles, zIndex: 15 }}
          items={suggestedHouseNumbers}
          renderInput={props => <TextField label={text.HouseNumber} type="text" autoComplete="nope" name="HouseNumber" {...props} disabled={!selectedStreet} />}
          renderItem={(item, isHighlighted) => (
            <div key={item.Id} style={{ background: isHighlighted ? 'lightgray' : 'white', padding: "10px 15px", borderBottom: "1px solid #e8e8e8" }}>
              {item.Text}
            </div>
          )}
        />

        <div className="sof-field-container col-md-6">
          <TextField label={text.AddressSuffix} disabled={!HouseNumber} id="FloorNumber" type="text" name="FloorNumber" />
        </div>
      </div>
      {
        displayedCompoundQuery && displayedCompoundQuery.length > 0
          ? <h4>Compound query: {displayedCompoundQuery}</h4>
          : null
      }
    </>
  )
}

export default App